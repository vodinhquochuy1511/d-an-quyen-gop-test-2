<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_SYSTEM')) {
    exit('Stop!!!');
}

define('NV_IS_MOD_PAGE', true);

function store_client($post, $databasename) {
    global $db;
    $value = implode("', '", $post);
    $column = implode(", ",array_keys($post));
    $sql = "INSERT INTO `$databasename`($column) VALUES ('$value')";
    print_r($sql);die;
    try {
        $db->query($sql);
        return true;
    } catch (PDOException $e) {
        return false;
    }
}

function update_client($id, $values, $table_name) {
    global $db;
    array_walk($values, function(&$value, $key) {
        $value = "{$key} = '{$value}'";
    });
    $sUpdate = implode(", ", array_values($values));
    $sql = "UPDATE `$table_name` SET $sUpdate WHERE `id` = $id";
    try {
        $db->query($sql);
        return true;
    } catch (PDOException $e) {
        return false;
    }
}

function request_client($post, $array_name) {
    $error = [];
    foreach ($post as $key => $value) {
        if(empty($value) || $value == 'undefined') {
            if(isset($array_name[$key])) {
                $error[] = $array_name[$key] . ' không được để trống!';
            }
        }
    }
    return $error;
}

function check_client($value, $post, $databasename) {
    global $db;
    $sql = "SELECT * FROM `$databasename` WHERE '$value' = '".$post."'";
    $data = $db->query($sql)->fetch();
    return $data;
}
