<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

include 'project.php';
global $admin_info;

$page_title = $lang_module['danhsachkhongduyet'];
$sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE is_duyet = 2';
$list = $db->query($sql)->fetchAll();

if($nv_Request->isset_request('delete', 'post,get')){
    $sql = 'DELETE FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $_POST['id'];
    $delete = $db->query($sql);
    if($delete){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

if($nv_Request->isset_request('chuyenveduan', 'post,get')){
    $_POST['is_duyet'] = 3;
    $change =  update($_POST['id'], $_POST,$table_duan);
    if($change){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

$xtpl = new XTemplate('danhsachkhongduyet.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
foreach($list as $key => $value){
    $value['hinh_anh'] = explode(',', $value['hinh_anh'])[0];
    $xtpl->assign('KEY', $key + 1);
    $xtpl->assign('VALUE', $value);
    $xtpl->parse('main.list_du_an');
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
