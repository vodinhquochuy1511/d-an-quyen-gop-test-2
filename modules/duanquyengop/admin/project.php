<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

$table_duan = 'nv4_vi_duanquyengop_themmoiduans';

$fillable = [
    'ten_du_an',
    'mo_ta_ngan',
    'mo_ta_chi_tiet',
    'so_tien',
    'hinh_anh',
    'thoi_han',
    'is_open',
];

$array_name = [
    'ten_du_an' => 'Tên dự án',
    'mo_ta_ngan' => 'Mô tả ngắn',
    'mo_ta_chi_tiet' => 'Mô tả chi tiết',
    'so_tien' => 'Số tiền',
    'hinh_anh' => 'Hình ảnh',
    'thoi_han' => 'Thời hạn',
    'is_open' => 'TÌnh trạng',
];