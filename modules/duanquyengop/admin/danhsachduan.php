<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

include 'project.php';
global $admin_info;

$page_title = $lang_module['danhsachduan'];

$sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE is_duyet != 2';
$list = $db->query($sql)->fetchAll();

if($nv_Request->isset_request('change', 'post,get')){
    $check = check('id', $_POST['id'], $table_duan);
    // print_r($check['is_open']);die;
    if($check['is_open'] == 1){
        $_POST['is_open'] = 2;
    }else{
        $_POST['is_open'] = 1;
    }
    $change =  update($_POST['id'], $_POST,$table_duan);
    if($change){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

if($nv_Request->isset_request('duyet', 'post,get')){
    $data = check('id', $_POST['id'], $table_duan);
    if($data){
        nv_jsonOutput([
            'data' => $data,
        ]);
    }
}

if($nv_Request->isset_request('acceptMaduan', 'post,get')){
    if(empty($_POST['ma_du_an'])){
        nv_jsonOutput([
            'status' => false,
            'mess'  => 'Phải nhập mã dự án !'
        ]);
    }else{
        $_POST['is_duyet'] = 1;
        $change =  update($_POST['id'], $_POST,$table_duan);
        if($change){
            nv_jsonOutput([
                'status' => true,
            ]);
        }
    }
}

if($nv_Request->isset_request('delete', 'post,get')){
    $sql = 'DELETE FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $_POST['id'];
    $delete = $db->query($sql);
    if($delete){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

if($nv_Request->isset_request('unduyet', 'post,get')){
    $_POST['is_duyet'] = 3;
    $change =  update($_POST['id'], $_POST,$table_duan);
    if($change){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}

if($nv_Request->isset_request('khongduyet', 'post,get')){
    $_POST['is_duyet'] = 2;
    $change =  update($_POST['id'], $_POST,$table_duan);
    if($change){
        nv_jsonOutput([
            'status' => true,
        ]);
    }
}


$xtpl = new XTemplate('danhsachduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
foreach($list as $key => $value){
    $value['hinh_anh'] = explode(',', $value['hinh_anh'])[0];
    if($value['is_open'] == 1){
        $x = '<button class="change btn btn-primary" data-id="'. $value['id'] .'">Hiển thị</button>';
        $xtpl->assign('IS_OPEN', $x);
    }else{
        $x = '<button class="change btn btn-danger" data-id="'. $value['id'] .'">Tạm tắt</button>';
        $xtpl->assign('IS_OPEN', $x);
    }

    if($value['is_duyet'] == 3){
        $x = '<button class="duyet btn btn-primary" data-id="'. $value['id'] .'" data-toggle="modal" data-target="#duyetModal" >Duyệt</button>
              <button class="khongduyet btn btn-warning" data-id="'. $value['id'] .'" >Không duyệt</button>';
        $xtpl->assign('IS_DUYET', $x);
    }else{
        $x = '<button class="unduyet btn btn-success" data-id="'. $value['id'] .'" >Đã duyệt</button>';
        $xtpl->assign('IS_DUYET', $x);
    }
    
    $xtpl->assign('KEY', $key + 1);
    $xtpl->assign('VALUE', $value);
    $xtpl->parse('main.list_du_an');
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_admin_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
