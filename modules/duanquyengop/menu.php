<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_FILE_ADMIN')) {
    exit('Stop!!!');
}

$row = [
    [ 
        'id' => 2,
        'title' => 'Tạo dự án',
        'alias' => 'taoduan',
    ],
    [ 
        'id' => 3,
        'title' => 'Lịch sử dự án',
        'alias' => 'lichsugiaodich',
    ],
    [ 
        'id' => 4,
        'title' => 'Card Profile',
        'alias' => 'card_profile',
    ],
    [ 
        'id' => 5,
        'title' => 'Card',
        'alias' => 'card',
    ],
];

foreach($row as $key => $value)
    $array_item[$value['id']] = [
        'key' => $value['id'],
        'title' => $value['title'],
        'alias' => $value['alias'] . $global_config['rewrite_exturl']
    ];

