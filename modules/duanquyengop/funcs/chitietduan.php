<?php

/**
 * NukeViet Content Management System
 * @version 4.x
 * @author VINADES.,JSC <contact@vinades.vn>
 * @copyright (C) 2009-2021 VINADES.,JSC. All rights reserved
 * @license GNU/GPL version 2 or any later version
 * @see https://github.com/nukeviet The NukeViet CMS GitHub project
 */

if (!defined('NV_IS_MOD_PAGE')) {
    exit('Stop!!!');
}
$id = $nv_Request->get_int('id','post,get');
$sql = 'SELECT * FROM `nv4_vi_duanquyengop_themmoiduans` WHERE id=' . $id;
$chitietduan = $db->query($sql)->fetch();
$hinh_anh = explode(',', $chitietduan['hinh_anh']);
$xtpl = new XTemplate('chitietduan.html', NV_ROOTDIR . '/themes/' . $global_config['module_theme'] . '/modules/' . $module_file);
$xtpl->assign('V', $chitietduan);

foreach($hinh_anh as $key => $value){
    if($key == 0){
        $x = 'item active';
        $xtpl->assign('ACTIVE', $x);
    }else{
        $x = 'item';
        $xtpl->assign('ACTIVE', $x);
    }
    $xtpl->assign('H', $value);
    $xtpl->parse('main.hinh_anh');
}

$xtpl->parse('main');
$contents = $xtpl->text('main');

include NV_ROOTDIR . '/includes/header.php';
echo nv_site_theme($contents);
include NV_ROOTDIR . '/includes/footer.php';
